﻿<%@ Register TagPrefix="Footer" TagName="TagFooter" src="~/Page-Components/footer.ascx" %>
<%@ Register TagPrefix="Header" TagName="TagHeader" src="~/Page-Components/header.ascx" %>
<%@ Register TagPrefix="ContactForm" TagName="TagContactForm" src="~/Page-Components/contact-form.ascx" %>
<%@ Register TagPrefix="Head" TagName="TagHead" src="~/Page-Components/head.ascx" %>

<!DOCTYPE html>
<!--[if IE 7]>         <html class="ie7"> <![endif]-->
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if gt IE 8]>--> 
<html>

	<Head:TagHead id="Head" title="head" runat="server"/>

	<body>
		<form id="Form1" runat="server">
			<Header:TagHeader id="Header" title="header title" runat="server"/>

			<div class="content-title-strip">
				<div class="inner-wrapper">
					<div class="content-title">
						<div class="title-text">
							<h1>Something useful to say?</h1><p></p>
							<p><h3>I'd love to hear from you. I might reply if the message is worthy.</h3><p>
						</div>
					</div>
					<div class="content-title-image">
						<img src="/Styles/images/header-images/contact.png" alt="about-us icon"/>
					</div>
				</div>
			</div>
			
			<ContactForm:TagContactForm id="ContactForm" title="contact form" runat="server"/>
			
			<Footer:TagFooter id="Footer" title="footer title" runat="server"/>
		</form>
	</body>
</html>