﻿<%@ Register TagPrefix="Footer" TagName="TagFooter" src="~/Page-Components/footer.ascx" %>
<%@ Register TagPrefix="Header" TagName="TagHeader" src="~/Page-Components/header.ascx" %>
<%@ Register TagPrefix="Slider" TagName="TagSlider" src="~/Page-Components/image-slider.ascx" %>	
<%@ Register TagPrefix="Teasers" TagName="TagTeasers" src="~/Page-Components/home-teaser.ascx" %>
<%@ Register TagPrefix="Head" TagName="TagHead" src="~/Page-Components/head.ascx" %>

<!DOCTYPE html>
<!--[if IE 7]>         <html class="ie7"> <![endif]-->
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if gt IE 8]>--> 
<html>

	<Head:TagHead id="Head" title="head" runat="server"/>

	<body>
		<form id="Form1" runat="server">
			<Header:TagHeader id="Header" title="header title" runat="server"/>
			
			<!-- Image rotator start -->
			<Slider:TagSlider id="Slider" title="header title" runat="server"/>
			
			<!-- <p>Body Content</p> -->
			
			<script>
			    $(window).load(function () {
			        $('#slider').nivoSlider({
			            directionNavHide: false,
			            captionOpacity: 1,
			            prevText: '<',
			            nextText: '>'
			        });
			    });
			</script>
			<!-- Image rotator end -->
			
			<Teasers:TagTeasers id="Teasers" title="teasers" runat="server"/>
			
			<Footer:TagFooter id="Footer" title="footer title" runat="server"/>
		</form>
	</body>
</html>