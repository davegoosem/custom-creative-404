﻿<%@ Register TagPrefix="Footer" TagName="TagFooter" src="~/Page-Components/footer.ascx" %>
<%@ Register TagPrefix="Header" TagName="TagHeader" src="~/Page-Components/header.ascx" %>
<%@ Register TagPrefix="Head" TagName="TagHead" src="~/Page-Components/head.ascx" %>

<!DOCTYPE html>
<!--[if IE 7]>         <html class="ie7"> <![endif]-->
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if gt IE 8]>--> 
<html>

	<Head:TagHead id="Head" title="head" runat="server"/>

	<body>
		<form id="Form1" runat="server">
			<Header:TagHeader id="Header" title="header title" runat="server"/>
			
			<div class="content-title-strip">
				<div class="inner-wrapper">
					<div class="content-title">
						<div class="title-text">
							<h1>A bit about the site</h1>
							<p><h3>Who, what, when, why, how - or at least some of these answered here.</h3></p>
						</div>
					</div>
					<div class="content-title-image">
						<img src="/Styles/images/header-images/about-us.png" alt="about-us icon"/>
					</div>
				</div>
			</div>
			
			<div class="login-form">
				<div class="paypal-form">

					<p><h2>What is a 404 page and what's the point of this site?</h2></p>
					
					<p>A 404 page, also known as the 'not found' page, is the page a veiwer of a website will see when the server cannot find what was asked of it so long as communication with the server is working. All good websites should ideally have one although some will just be a default page. </p>

					<p> The aim of the site is to be a place where people can come and view humerous/interesting/awesome/creative custom 404 pages from all over the web. You, as a user can register and join the community, which will allow you to comment on and submit other 404 pages for review on the site.</p>

					<p> Website owners can use the site to gain some 'street cred' for their site as 404 pages are typically where even a serious business site can have a bit of fun and show their personality.  </p>

					<br />

					<p><h2>Some interesting facts about the site</h2></p>

					<p> This site has been developed as a bit of a playground for trying new things with various technologies and web languages.<p>

					<p>Having not made a fully functional site before with any of these languages, it has slowly taken shape and is only being developed in my spare time. It will continue to be developed for as long as it keeps me interested.</p>
					
					<p>The site is currently publically viewable on GitHub <a href="https://github.com/DaveGoosem/Custom-Creative-404" target="_blank">here.</a></p>

				</div>

			</div>

			<Footer:TagFooter id="Footer" title="footer title" runat="server"/>
		</form>
	</body>
</html>