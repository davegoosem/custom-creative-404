<!-- login-form.js -->
$(document).ready(function(){
	//Check if Javascript Enabled
	$('body').addClass('js');

	//make the checkbox checked on load
	// Make the checkbox checked on load
    $('.login-form span').addClass('checked').children('input').attr('checked', true);

    //Click function
	$('.login-form span').on('click', function() 
	{

		if ($(this).children('input').attr('checked'))
		{
			$(this).children('input').attr('checked', false);
			$(this).removeClass('checked');
		}

		else 
		{
			$(this).children('input').attr('checked', true);
			$(this).addClass('checked');
		}

	});
});
