#Creative404.com Project

## About this site

This site has been developed as a bit of a playground for trying new things out and as a first round attempt at creating my own custom site with various technologies and web languages.

Having not made a fully functional site before with any of these languages, it is slowly taking shape and is only being developed in my spare time.

---------------------------------------
## Aim / Goals of the site

The aim of the site (aside from to look damn hot), is to be a place where people can come and view humerous/interesting/awesome/creative 404 pages from all over the web. Users will be able to register and join the community, allowing them to comment on and submit other 404 pages for review on the site.

Website owners can use the site to gain some 'street cred' for their site as 404 pages are typically where even a serious business site can have a bit of fun and show their personality.

---------------------------------------
### Technologies used:
- ASP .NET (C#)
- CSS .LESS
- MS SQL

### Software used in development:
- Sublime Text
- Visual Studio 2010
- Adobe Photoshop
- Github repo, using new GUI tool
- MS SQL Server