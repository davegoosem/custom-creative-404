﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using Selenium;
using System.IO;

namespace SeleniumTests
{
    [TestFixture]
    public class TestLinkPages
    {
        private ISelenium selenium;
        private StringBuilder verificationErrors;



            selenium.Open("/home.aspx");
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Gallery")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }

            selenium.Click("link=Gallery");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=About")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            selenium.Click("link=About");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Contact")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Contact");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Register")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            

            selenium.Click("link=Register");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Donate")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Donate");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Contact")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=div.span5.footer-links > ul > li > a");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Contact")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Change Password");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Forgot Password")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Forgot Password");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("xpath=(//a[contains(text(),'Register')])[2]")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("xpath=(//a[contains(text(),'Register')])[2]");
            selenium.WaitForPageToLoad("30000");
            
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Submit your page")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Submit your page");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=View the gallery")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=View the gallery");
            selenium.WaitForPageToLoad("30000");
            
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("xpath=(//a[contains(text(),'Contact')])[2]")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("xpath=(//a[contains(text(),'Contact')])[2]");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("css=span > a")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=span > a");
            selenium.WaitForPageToLoad("30000");
            
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("xpath=(//a[contains(text(),'Donate')])[2]")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("xpath=(//a[contains(text(),'Donate')])[2]");
            selenium.WaitForPageToLoad("30000");
            
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("xpath=(//a[contains(text(),'Gallery')])[2]")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("xpath=(//a[contains(text(),'Gallery')])[2]");
            selenium.WaitForPageToLoad("30000");
            
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Header_lnkLogin")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("id=Header_lnkLogin");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Change password")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Change password");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("css=a.teaser-top-link")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=a.teaser-top-link");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("css=h2 > strong")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=h2 > strong");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=View the Gallery")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=View the Gallery");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("css=figure.teaser-icon.teaser2 > a.teaser-top-link")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=figure.teaser-icon.teaser2 > a.teaser-top-link");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("//form[@id='Form1']/div[5]/section[2]/a/header/h2/strong")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
                       
            selenium.Click("//form[@id='Form1']/div[5]/section[2]/a/header/h2/strong");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Submit Your Page")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Submit Your Page");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("css=figure.teaser-icon.teaser3 > a.teaser-top-link")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=figure.teaser-icon.teaser3 > a.teaser-top-link");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("//form[@id='Form1']/div[5]/section[3]/a/header/h2/strong")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("//form[@id='Form1']/div[5]/section[3]/a/header/h2/strong");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Register on the site")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Register on the site");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("css=figure.teaser-icon.teaser4 > a.teaser-top-link")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("css=figure.teaser-icon.teaser4 > a.teaser-top-link");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("//form[@id='Form1']/div[5]/section[4]/a/header/h2/strong")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("//form[@id='Form1']/div[5]/section[4]/a/header/h2/strong");
            selenium.WaitForPageToLoad("30000");
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Make a Donation")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
            
            selenium.Click("link=Make a Donation");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsElementPresent("link=Home")) break;
                }
                catch (Exception)
                {
                    Console.Write("Coudn't find required element to continue");
                }
                Thread.Sleep(1000);
            }
