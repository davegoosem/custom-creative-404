﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using Selenium;
using System.IO;

namespace SeleniumTests
{
    [TestFixture]
    public class Registration_Test
    {
        private ISelenium selenium;
        private StringBuilder verificationErrors;

        [SetUp]
        public void SetupTest()
        {
            selenium = new DefaultSelenium("localhost", 4444, "*googlechrome", "http://custom-creative-404-dropbox:8080/");
            selenium.Start();
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                selenium.Stop();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
                Console.Write("Unable to Stop Selenium and/or close the browser");
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void Registration()
        {
            
            completeRegistrationForm();

            //back to home page.
            selenium.Click("link=Home");
            selenium.WaitForPageToLoad("30000");
        }


        public void completeRegistrationForm()
        {
            selenium.Open("/");
            selenium.Click("link=Register");
            selenium.WaitForPageToLoad("30000");

            string uniqueVal = selenium.GetEval("\"AutomatedTest\" + (new Date().getDate()) + (new Date().getTime())");
            //use moment instead.

            selenium.Type("id=RegistrationForm_TxtName", uniqueVal);

            selenium.Type("id=RegistrationForm_TxtUserName", uniqueVal + "@creative-404.com");
            selenium.Type("id=RegistrationForm_TxtPassword", uniqueVal);
            selenium.Type("id=RegistrationForm_TxtRePassword", uniqueVal);
            selenium.Click("id=RegistrationForm_Button1");
            selenium.WaitForPageToLoad("30000");

            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (selenium.IsTextPresent("Registration was successful!")) break;
                }
                catch (Exception)
                {
                    Console.Write("Unable to find the Registration successful message. Something has gone wrong..");
                }
                Thread.Sleep(1000);
            }
        }

    }
}
