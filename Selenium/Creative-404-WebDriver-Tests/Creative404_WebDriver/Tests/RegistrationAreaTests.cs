﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Creative404_WebDriver;
using Creative404_WebDriver.Classes;
using Creative404_WebDriver.Pages;

namespace Creative404_WebDriver.Tests
{
    [TestFixture]
    public class RegistrationAreaTests
    {
        private IWebDriver _driver;
        private StringBuilder _verificationErrors;
        private Browser _browser;
        private RegistrationAreaPage _registrationArea;
        private string _baseURL;

        [SetUp]
        public void SetupTest()
        {
            _driver = new FirefoxDriver();
            _baseURL = "http://custom-creative-404-dropbox:8080/";
            _verificationErrors = new StringBuilder();
            _browser = new Browser(_driver, _verificationErrors);
        }

        [TearDown]
        public void TeardownTest()
        {
            _browser.TeardownTest();                       
        }

        [Test(Description = "Test to ensure that you can register a new user with valid data")]
        public void CanRegisterWithValidData()
        {
            _driver.Navigate().GoToUrl(_baseURL + "/Account/Register.aspx");
            _registrationArea.FillOutRegistrationForm();
            _registrationArea.SubmitRegistrationForm();
            try
            {
                Assert.AreEqual("Registration was successful!", _driver.FindElement(By.Id("RegistrationForm_lblRegistrationMessage")).Text);
            }
            catch (AssertionException e)
            {
                _verificationErrors.Append(e.Message);
            }
        }


        private bool IsElementPresent(By by)
        {
            try
            {
                _driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

    }
}
