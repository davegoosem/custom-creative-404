﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Creative404_WebDriver.Classes;

namespace Creative404_WebDriver.Pages
{
    public class RegistrationAreaPage
    {
        private IWebDriver _driver;
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        
        public RegistrationAreaPage(IWebDriver driver)
        {

            _driver = driver;
            Name = "AutoUser" + DateTime.Now.ToString("yyyyMMddhhmmss");
            Email = "AutoUser" + DateTime.Now.ToString("yyyyMMddhhmmss");
            Password = "Testing123!";
            RePassword = "Testing123!";
           
        }

        #region "Get Content Methods"

        public string GetName()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtName")).Text;
        }

        public string GetEmail()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtUserName")).Text;
        }

        public string GetPassword()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtPassword")).Text;
        }

        public string GetRePassword()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtRePassword")).Text;
        }

        #endregion


        #region "Error Methods"

        public bool NameHasError()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtName")) != null;
        }

        public bool EmailHasError()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtUserName")) != null;
        }

        public bool PasswordHasError()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtPassword")) != null;
        }

        public bool RePasswordHasError()
        {
            return _driver.FindElement(By.Id("RegistrationForm_TxtRePassword")) != null;
        }

        #endregion

        #region "Fill out form methods"

        public RegistrationAreaPage FillOutRegistrationForm()
        {
            FillOutName();
            FillOutEmail();
            FillOutPassword();
            FillOutRePassword();
            return this;
        }

        public RegistrationAreaPage FillOutName(string name = null)
        {
            if (name != null) Name = name;
            _driver.FindElement(By.Id("RegistrationForm_TxtName")).Clear();
            _driver.FindElement(By.Id("RegistrationForm_TxtName")).SendKeys(Name);
            return this;
        }


        public RegistrationAreaPage FillOutEmail(string email = null)
        {
            if (email != null) Email = email;
            _driver.FindElement(By.Id("RegistrationForm_TxtUserName")).Clear();
            _driver.FindElement(By.Id("RegistrationForm_TxtUserName")).SendKeys(Email);
            return this;
        }

        public RegistrationAreaPage FillOutPassword(string password = null)
        {
            if (password != null) Password = password;
            _driver.FindElement(By.Id("RegistrationForm_TxtPassword")).Clear();
            _driver.FindElement(By.Id("RegistrationForm_TxtPassword")).SendKeys(Password);
            return this;
        }

        public RegistrationAreaPage FillOutRePassword(string rePassword = null)
        {
            if (rePassword != null) RePassword = rePassword;
            _driver.FindElement(By.Id("RegistrationForm_TxtRePassword")).Clear();
            _driver.FindElement(By.Id("RegistrationForm_TxtRePassword")).SendKeys(RePassword);
            return this;
        }



        #endregion
        
        #region "Functionality Methods"

        public RegistrationAreaPage SubmitRegistrationForm()
        {
            _driver.FindElement(By.Id("RegistrationForm_Button1")).Click();
            return new RegistrationAreaPage(_driver);
        }

        #endregion


        private bool IsElementPresent(By by)
        {
            try
            {
                _driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
