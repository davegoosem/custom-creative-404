﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace Creative404_WebDriver.Classes
{
    public class Browser
    {
        private IWebDriver _driver;
        private StringBuilder _verificationErrors;


        public Browser(IWebDriver driver, StringBuilder verificationErrors)
        {
            _driver = driver;
            _verificationErrors = verificationErrors;
        }


        public void TeardownTest()
        {
            try
            {
                _driver.Quit();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to execute the TearDown step.");
            }
            Assert.AreEqual("", _verificationErrors.ToString());
        }


    }
}
