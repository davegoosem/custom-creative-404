#Sql server database backup:

1. Create login: custom-creative-404
2. Create new database (if first time) named 'custom-creative-404'
3. Change the db backup to have a .bak extension instead of .db
4. restore backup over the top
5. ensure the user 'custom-creative-404' is set up correctly and away you go.

-----------------------------

6. If need be, use this query on the database:

`EXEC sp_change_users_login 'Update_One', 'custom-creative-404' , 'custom-creative-404'`

(It uses the (user), (login) in that order)

