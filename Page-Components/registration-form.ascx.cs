﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Security.Cryptography;

namespace creative_404.Page_Components
{
	public partial class registration_form : System.Web.UI.UserControl
	{

		private void ExecuteInsert(string name, string username, string password)
		{
		    SqlConnection conn = new SqlConnection(GetConnectionString());
		    string sql = "INSERT INTO Users (Name, UserName, Password) VALUES "+ " (@Name,@UserName,@Password)";

		    try
		    {
		        conn.Open();
		        SqlCommand cmd = new SqlCommand(sql, conn);
		        SqlParameter[] param = new SqlParameter[3];

		        param[0] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
		        param[1] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
		        param[2] = new SqlParameter("@Password", SqlDbType.VarChar, 50);

		        
		        param[0].Value = name;
		        param[1].Value = username;
		        param[2].Value = CalculateMD5Hash(password);


		        for (int i = 0; i < param.Length; i++)
		        {
		            cmd.Parameters.Add(param[i]);
		        }

		        cmd.CommandType = CommandType.Text;
		        cmd.ExecuteNonQuery();
		    }
		    catch (System.Data.SqlClient.SqlException ex)
		    {
		        string msg = "Insert Error:";
		        msg += ex.Message;
		        throw new Exception(msg);
		    }
		    finally
		    {
		        conn.Close();
		    }
		}


		protected void Button1_Click(object sender, EventArgs e)
		{
			string checkExistsQuery = "SELECT UserName FROM Users WHERE Username = @username";

			SqlConnection conn = new SqlConnection(GetConnectionString());
			SqlCommand checkExistsCmd = new SqlCommand(checkExistsQuery, conn);

			SqlParameter c1 = new SqlParameter("username", TxtUserName.Text);

			checkExistsCmd.Parameters.Add(c1);

			conn.Open();

			SqlDataReader rd = checkExistsCmd.ExecuteReader();
			    
			if (rd.HasRows)
			{
				lblRegistrationMessage.Text="Oops, a user with that email already exists.";
				rd.Close();
				conn.Close();
			}
			else
			{
				 if (TxtPassword.Text == TxtRePassword.Text)
			    {
			        //call the method to execute insert to the database
			        ExecuteInsert(TxtName.Text, TxtUserName.Text, TxtPassword.Text);

			        lblRegistrationMessage.Text="Registration was successful!";
			        ClearControls(Page);
			    }
			    else
			    {
			        lblRegistrationMessage.Text="Password did not match!";
			        TxtPassword.Focus();
			    }

				rd.Close();
				conn.Close();
			}

		}

		public static void ClearControls(Control Parent)
		{
		    if (Parent is TextBox)
		    { 
		    	(Parent as TextBox).Text = string.Empty; 
		    }
		    else
		    {
		        foreach (Control c in Parent.Controls)
		            ClearControls(c);
		    }
		}

		public string CalculateMD5Hash(string input)
		{
		    // step 1, calculate MD5 hash from input
		    MD5 md5 = System.Security.Cryptography.MD5.Create();
		    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
		    byte[] hash = md5.ComputeHash(inputBytes);
		 
		    // step 2, convert byte array to hex string
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < hash.Length; i++)
		    {
		        sb.Append(hash[i].ToString("X2"));
		    }
		    return sb.ToString();
		}

		public string GetConnectionString()
		{
			return System.Configuration.ConfigurationManager.ConnectionStrings["Creative404ConnectionString"].ConnectionString;
		}

	}
}