<!-- Change-Password-form.aspx -->
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Change-Password-Form.ascx.cs" Inherits="creative_404.Page_Components.Change_Password_Form" %>


<div class="login-form">
    <div class="form-title">
        <h2>Please complete the form below to change your password.</h2>
        <p class="form-text">Registering will allow you to post comments and submit pages for consideration on the site.</p>
    </div>

    <div class="form-wrapper">
        <div class="control-group">
            <span class="form-label">Email:</span>
                <asp:TextBox ID="TxtUserName" TabIndex="2" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RegularExpressionValidator ID="EmailRegExValidator" runat="server" ControlToValidate="TxtUserName" ErrorMessage="Please enter a valid email address." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" ValidationGroup="vgChangePassword"> 
                    </asp:RegularExpressionValidator>
                </span>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="EmailNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtUserName" runat="server" ErrorMessage="Your email address is required." ValidationGroup="vgChangePassword">
                    Your email address is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>

        <div class="control-group">
             <span class="form-label">Old Password:</span>
                <asp:TextBox ID="TxtOldPassword" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="200" TextMode="Password"></asp:TextBox>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="OldPasswordNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtOldPassword" runat="server"  ErrorMessage="Old password is required." ValidationGroup="vgChangePassword">
                    Old password is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

         <div class="control-group">
             <span class="form-label">New Password:</span>
                <asp:TextBox ID="TxtNewPassword" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="200" TextMode="Password"></asp:TextBox>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="PasswordNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtNewPassword" runat="server"  ErrorMessage="A password is required." ValidationGroup="vgChangePassword">
                    New password is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

       <div class="control-group">
             <span class="form-label">Retype New Password:</span>
                <asp:TextBox ID="TxtRePassword" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="200" TextMode="Password"></asp:TextBox>
                <span class="validation-text">
                    <asp:CompareValidator ID="RePasswordConfirmation" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtRePassword" ControlToCompare="TxtNewPassword" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="vgChangePassword">
                    Password confirmation does not match new password.
                    </asp:CompareValidator>
                </span>    
        </div>
        
        <asp:Button ID="ChangePasswordButton" runat="server" CssClass="form-button" Text="Submit" onclick="ChangePasswordButton_Click" ValidationGroup="vgChangePassword"/>

        <asp:label class="validation-text" id="lblChangePasswordMessage" runat="server"></asp:label>

    </div>
</div>