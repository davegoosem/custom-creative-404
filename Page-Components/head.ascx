﻿<head id="Head1" class="head1" runat="server">

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

	<title id="Title1" runat="server">Creative 404</title>

	<link rel="stylesheet/less" href="/Styles/styles.less">
	<script src="/Scripts/less.js" type="text/javascript"></script>
	<script src="/Scripts/jquery.js" type="text/javascript"></script>
	<script src="/Scripts/jquery.nivo.slider.pack.js" type="text/javascript"></script>

	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-32626641-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>



