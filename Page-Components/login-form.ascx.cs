﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;

namespace creative_404.Page_Components
{
	public partial class login_form : System.Web.UI.UserControl
	{
	    protected void LoginButton_Click(object sender, EventArgs e)
		{
			SqlConnection conn = new SqlConnection(GetConnectionString());

			SqlCommand cmd = new SqlCommand("SELECT UserName, Password FROM Users WHERE Username = @username AND Password = @password", conn);

			SqlParameter p1 = new SqlParameter("username", TxtUserName.Text);
			SqlParameter p2 = new SqlParameter("password", CalculateMD5Hash(TxtPassword.Text));
			cmd.Parameters.Add(p1);
			cmd.Parameters.Add(p2);

			conn.Open();

			SqlDataReader rd = cmd.ExecuteReader();

			if (rd.HasRows)
			{
				rd.Read();

				Session["UserName"] = TxtUserName.Text;
				
				conn.Close();
				rd.Close();
				Response.Redirect(Request.RawUrl);
			}
			else
			{
				lblMessage.Text="Invalid username or password.";
			}
		}

		public string CalculateMD5Hash(string input)
		{
		    // step 1, calculate MD5 hash from input
		    MD5 md5 = System.Security.Cryptography.MD5.Create();
		    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
		    byte[] hash = md5.ComputeHash(inputBytes);
		 
		    // step 2, convert byte array to hex string
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < hash.Length; i++)
		    {
		        sb.Append(hash[i].ToString("X2"));
		    }
		    return sb.ToString();
		}

		public string GetConnectionString()
		{
			return System.Configuration.ConfigurationManager.ConnectionStrings["Creative404ConnectionString"].ConnectionString;
		}
	}
}