<%@ Control Language="C#" AutoEventWireup="true" CodeFile="image-gallery.ascx.cs" Inherits="creative_404.Page_Components.image_gallery" %>

<div class="gallery-wrapper">

	<asp:Repeater ID="RepeaterImages" runat="server">
		<ItemTemplate>
			<div class="image-thumb-wrapper">
				<div class="image-thumb">
					<asp:Image ID="Image1" runat="server" ImageUrl='<%# Container.DataItem %>' />
				</div>
				<div class="image-title">
					<h3> Image Title </h3>
				</div>
				<div class="image-description">
					Description goes here. Three lines long max is the length.
				</div>
				<div>					
					<img class="image-social" src="/Styles/images/gallery-icons/likes.png" alt="likes icon"/> 0 &nbsp; <img class="image-social" src="/Styles/images/gallery-icons/comments.png" alt="likes icon"/> 0
				</div>

			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>
