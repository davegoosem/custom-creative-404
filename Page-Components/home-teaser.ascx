﻿<div class="teaser-wrapper" class="clearfix">
	
	<!-- Teaser 1 : Gallery -->
	<section class="teaser-section">
		<figure class="teaser-icon teaser1">
			<a class="teaser-top-link" href="/gallery.aspx"></a>
		</figure>
		<a href="/gallery.aspx">
			<header>
				<h2>
					<strong>View the Gallery</strong>
				</h2>
			</header>
			<div class="teaser-blurb">
				<p>Click here to view a whole host of creative and interesting 404 pages from all over the net in the 404 Gallery</p>
			</div>
		</a>
		<a href="/gallery.aspx" class="teaser-bottom-link">View the Gallery</a>
	</section>
	<!-- End Teaser 1: Gallery -->
	
	<!-- Teaser 2: Submit your page -->
	<section class="teaser-section" >
		<figure class="teaser-icon teaser2"> <!-- this will be a nice icon image -->
			<a class="teaser-top-link" href="/submit-your-page.aspx"></a>
		</figure>
		<a href="/submit-your-page.aspx">
			<header>
				<h2>
					<strong>Submit Your Page</strong>
				</h2>
			</header>
			<div class="teaser-blurb">
				<p>Click here to submit your own page for consideration to be included on this site</p>
			</div>
		</a>
		<a href="/submit-your-page.aspx" class="teaser-bottom-link">Submit Your Page</a>
	</section>
	<!-- End Teaser 2: Teaser 2: Submit your page -->

	<!-- Teaser 3: Register -->
	<section class="teaser-section">
		<figure class="teaser-icon teaser3"> <!-- this will be a nice icon image -->
			<a class="teaser-top-link" href="/Account/Register.aspx"></a>
		</figure>
		<a href="/Account/Register.aspx">
			<header>
				<h2>
					<strong>Click to Register</strong>
				</h2>
			</header>
			<div class="teaser-blurb">
				<p>By registering, you can join the community here. Comment on your favourites and offer sugestions</p>
			</div>
		</a>
		<a href="Account/Register.aspx" class="teaser-bottom-link">Register on the site</a>
	</section>
	<!-- End Teaser 3: Register -->

	<!-- Teaser 4: Donate -->
	<section class="teaser-section">
		<figure class="teaser-icon teaser4"> <!-- this will be a nice icon image -->
			<a class="teaser-top-link" href="/donate.aspx"></a>
		</figure>
		<a href="/donate.aspx">
			<header>
				<h2>
					<strong>Buy me a beer!</strong>
				</h2>
			</header>
			<div class="teaser-blurb">
				<p>Like what you see on the site? It's thirsty work.. Buy me a beer and I might just keep working on it!</p>
			</div>
		</a>
		<a href="/donate.aspx" class="teaser-bottom-link">Make a Donation</a>
	</section>
	<!-- End Teaser 4: Donate -->

</div>

<!-- /end #services -->