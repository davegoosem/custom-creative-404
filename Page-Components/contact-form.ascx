<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contact-form.ascx.cs" Inherits="creative_404.Page_Components.contact_form" %>
<!-- header ASP.NET control -->

<div class="login-form">
    <div class="form-title">
        <h2>To get in contact, shoot me an email here.</h2>
    </div>

    <div class="form-wrapper">
        <div class="control-group">
            <span class="form-label">Name:</span>
                <asp:TextBox ID="TxtContactName" TabIndex="1" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="ContactNameNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtContactName" runat="server" ErrorMessage="Your name is required." ValidationGroup="vgContact">
                    Your name is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

        <div class="control-group">
            <span class="form-label">Email:</span>
                <asp:TextBox ID="TxtContactEmail" TabIndex="2" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RegularExpressionValidator ID="ContactEmailRegExValidator" runat="server" ControlToValidate="TxtContactEmail" ErrorMessage="Please enter a valid email address." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" ValidationGroup="vgContact"> 
                    </asp:RegularExpressionValidator>
                </span>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="ContactEmailNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtContactEmail" runat="server" ErrorMessage="Your email address is required." ValidationGroup="vgContact">
                    Your email address is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

         <div class="control-group">
             <span class="form-label">Comments:</span>
                <asp:TextBox ID="TxtComments" TabIndex="3" runat="server" CssClass="NormalTextBox" TextMode="MultiLine" Rows="10" Width="400"></asp:TextBox>
            </div>
                <span class="validation-text">
                    <div style="margin-right: 200px">
                    <asp:RequiredFieldValidator ID="CommentsNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtComments" runat="server"  ErrorMessage="Please include some content in the Comments field." ValidationGroup="vgContact">
                    Please include some content in the Comments field.
                    </asp:RequiredFieldValidator>
                </div>
                </span>        
        <asp:Button ID="Button1" runat="server" CssClass="form-button" Text="Submit" onclick="Button1_Click" ValidationGroup="vgContact"/>

    </div>
</div>
