<%@ Control Language="C#" AutoEventWireup="true" CodeFile="submit-page-form.ascx.cs" Inherits="creative_404.Page_Components.submit_page_form" %>

<div class="login-form">
    <div class="form-title">
        <h2>Enter the website name as you'd like it to appear in the gallery.</h2>
        <p class="form-text">Each submission requires the following:</p>
        <p class="form-text">1. Website name: This is the name of the website that your submission is from. Eg. Creative-404.com</p>
        <p class="form-text">2. Thumbnail Image: This is a .jpg thumbnail image which is already sized to 140px X 120px.</p>
        <p class="form-text">3. Full Sized Image: This is the full sized image which will display in the gallery. </p>
    </div>

    <div class="form-wrapper">
         <div class="control-group">
            <span class="form-label">Website name:</span>
            <asp:TextBox ID="TxtWebsite" TabIndex="1" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
            <span class="validation-text">
                <asp:RequiredFieldValidator ID="WebsiteNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtWebsite" runat="server"  ErrorMessage="A website is required." ValidationGroup="vgSubmitImages">
                Website name is required.
                </asp:RequiredFieldValidator>
            </span>
        </div>

    	<div class="control-group">
            <span class="form-label">Thumbnail image:</span>
            <asp:FileUpload ID="inputThumbImage" TabIndex="2" runat="server" CssClass="NormalTextBox" Width="250"></asp:FileUpload>
            <span class="validation-text">
                <asp:RequiredFieldValidator ID="ThumbNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="inputThumbImage" runat="server"  ErrorMessage="A thumbnail sized image is required." ValidationGroup="vgSubmitImages">
                A thumbnail sized image is required.
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="ThumbFileTypeValidate" ControlToValidate="inputThumbImage" ErrorMessage="Invalid File. Must be .jpg" ValidationExpression="^.*\.([j|J][p|P][g|G])$" />
            </span>
        </div>

		<div class="control-group">
            <span class="form-label">Full sized image:</span>
            <asp:FileUpload ID="inputFullImage" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="250"></asp:FileUpload>
            <span class="validation-text">
                <asp:RequiredFieldValidator ID="FullSizeNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="inputFullImage" runat="server"  ErrorMessage="A full sized image is required." ValidationGroup="vgSubmitImages">
                A full sized image is required.
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="FullFileTypeValidate" ControlToValidate="inputFullImage" ErrorMessage="Invalid File. Must be .jpg" ValidationExpression="^.*\.([j|J][p|P][g|G])$" />  
            </span>
        </div>                
       
        <asp:Button ID="SubmitButton" runat="server" CssClass="form-button" Text="Submit" onclick="SubmitButton_Click" ValidationGroup="vgSubmitImages"></asp:Button>

        <asp:label class="validation-text" id="lblMessage" runat="server"></asp:label>
    </div>
</div>