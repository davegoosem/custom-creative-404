//Change-Password-Form.aspx.cs

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;

namespace creative_404.Page_Components
{
	public partial class Change_Password_Form : System.Web.UI.UserControl
	{
		protected void ChangePasswordButton_Click(object sender, EventArgs e)
		{
			
			if (Session["UserName"] != null)
			{
				SqlConnection conn = new SqlConnection(GetConnectionString());

				string checkExistsQuery = "SELECT UserName, Password FROM Users WHERE Username = @username AND Password = @password";
				string updatePasswordQuery = "UPDATE Users SET Password = @newpassword WHERE Username = @username AND Password = @password";

				SqlCommand checkExistsCmd = new SqlCommand(checkExistsQuery, conn);

				SqlParameter c1 = new SqlParameter("username", TxtUserName.Text);
				SqlParameter c2 = new SqlParameter("password", CalculateMD5Hash(TxtOldPassword.Text));

				checkExistsCmd.Parameters.Add(c1);
				checkExistsCmd.Parameters.Add(c2);

				conn.Open();

				SqlDataReader rd = checkExistsCmd.ExecuteReader();
			    
				if (rd.HasRows)
				{
					SqlCommand updatePasswordCmd = new SqlCommand(updatePasswordQuery, conn);

					SqlParameter u1 = new SqlParameter("username", TxtUserName.Text);
					SqlParameter u2 = new SqlParameter("password", CalculateMD5Hash(TxtOldPassword.Text));
					SqlParameter u3 = new SqlParameter("newpassword", CalculateMD5Hash(TxtNewPassword.Text));

					updatePasswordCmd.Parameters.Add(u1);
					updatePasswordCmd.Parameters.Add(u2);
					updatePasswordCmd.Parameters.Add(u3);

					rd.Close();
					updatePasswordCmd.ExecuteNonQuery();
					lblChangePasswordMessage.Text="Password changed successfully.";
					conn.Close();
				}
				else
				{
					lblChangePasswordMessage.Text="Invalid username or password.";
					rd.Close();
					conn.Close();
				}
			}
			else
			{
				lblChangePasswordMessage.Text="You must be logged in to change your password.";
			}
		}

		public string CalculateMD5Hash(string input)
		{
		    // step 1, calculate MD5 hash from input
		    MD5 md5 = System.Security.Cryptography.MD5.Create();
		    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
		    byte[] hash = md5.ComputeHash(inputBytes);
		 
		    // step 2, convert byte array to hex string
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < hash.Length; i++)
		    {
		        sb.Append(hash[i].ToString("X2"));
		    }
		    return sb.ToString();
		}


		public string GetConnectionString()
		{
			return System.Configuration.ConfigurationManager.ConnectionStrings["Creative404ConnectionString"].ConnectionString;

		}

	}
}