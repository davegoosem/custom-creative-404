using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Net.Mail;

namespace creative_404.Page_Components
{
    public partial class contact_form : System.Web.UI.UserControl
    {
        protected void SendMail()
        {
            // Email Address from where you send the mail
            var fromAddress = "contact.creative404@gmail.com";
            // any address where the email will be sending
            var toAddress = TxtContactEmail.Text.ToString(); 
            //Password of your gmail address
            const string fromPassword = "cRe@t1ve!";
            // Passing the values and make a email formate to display
            string subject = "Contact message from Creative404.com";
            string body = "From: " + TxtContactName.Text + "\n";
            body += "Email: " + TxtContactEmail.Text + "\n";
            body += "Subject: " + "\n";
            body += "Question: \n" + TxtComments.Text + "\n";
            // smtp settings
            var smtp = new System.Net.Mail.SmtpClient();
            {
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                smtp.Timeout = 20000;
            }
            // Passing values to smtp object
            smtp.Send(fromAddress, toAddress, subject, body);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //here on button click what will done 
                SendMail();
                TxtContactEmail.Text = "";
                TxtContactName.Text = "";
                TxtComments.Text = "";
            }
            catch (Exception) { }
        }
    }
}