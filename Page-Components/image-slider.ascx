﻿<!-- image-slider.ascx -->
<!-- need to link this file in the head of the page it's going into -->

<!-- Image slider width = 900px / height = 300px -->

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="image-slider.ascx.cs" Inherits="creative_404.Page_Components.image_slider" %>

<div class="mainSliderWrapper">

	<div class="slider-wrapper futurico-theme">
		<div id="slider" class="nivoSlider">

			<a href="mailchimp.com/stuff" target="_blank">
				<img src="/Styles/images/slider-images/404-MailChimp_20120425.jpg" alt="Mail Chimp's 404 Page" title="#caption1">
			</a>
			

			<a href="http://us.blizzard.com/en-us/i404" target="_blank">
				<img src="/Styles/images/slider-images/404-Blizzard_20120425.jpg" alt="Blizzard's 404 Page" title="#caption2">
			</a>
			
			<a href="http://www.deviantart.com/404" target="_blank">
				<img src="/Styles/images/slider-images/404-DeviantArt_20120425.jpg" alt="Deviant Art's 404 Page" title="#caption3">
			</a>

			<a href="http://www.4chan.org/404" target="_blank">
				<img src="/Styles/images/slider-images/404-4Chan_20120425.jpg" alt="4Chan's 404 Page" title="#caption4">
			</a>

		</div>

		<div id="caption1" class="nivo-html-caption">
			<strong> MailChimp.com: </strong><span></span><em>Mailchimp.com's 404 page.</em>
		</div>

		<div id="caption2" class="nivo-html-caption">
			<strong> Blizzard.com: </strong><span></span><em>Blizzard.com's 404 page.</em>
		</div>

		<div id="caption3" class="nivo-html-caption">
			<strong> DeviantArt.com: </strong><span></span><em>Deviantart.com's 404 page.</em>
		</div>

		<div id="caption4" class="nivo-html-caption">
			<strong> 4Chan.com: </strong><span> </span><em>4chan.com's 404 page.</em>
		</div>

	</div>
</div>