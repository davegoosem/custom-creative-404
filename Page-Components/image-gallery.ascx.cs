using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace creative_404.Page_Components
{
	public partial class image_gallery : System.Web.UI.UserControl
	{
	    protected void Page_Load(object sender, EventArgs e)
	    {

	    	string[] filesInDirectories = Directory.GetFiles(Server.MapPath("~/Gallery"));
	    	List<String> images = new List<string>(filesInDirectories.Count());

	    	foreach (string item in filesInDirectories)
	    	{
	    		images.Add(String.Format("~/Gallery/{0}", System.IO.Path.GetFileName(item)));
	    	}

	    	RepeaterImages.DataSource = images;
    		RepeaterImages.DataBind();
	    }


		//need to make my own pagination based on the stored proc on this tute: http://www.aspsnippets.com/Articles/Image-Gallery-using-ASP.Net-DataList-Control-Part-I---Pagination.aspx
		// Set variables for:
		//int pageNumber = 1;
		//int itemsPerPage = 12;

		//do a db read of all rows in the imagegallery db and get the total number of rows. etc. etc.

/*
		private int BindList(int PageNo)
		{
		    int TotalRows = 0;
		    DataTable dt = new DataTable();
		    String strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
		    SqlConnection con = new SqlConnection(strConnString);
		    SqlDataAdapter sda = new SqlDataAdapter();
		    SqlCommand cmd = new SqlCommand("spx_Pager");
		    
		    cmd.CommandType = CommandType.StoredProcedure;
		    cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;
		    cmd.Parameters.Add("@ItemsPerPage", SqlDbType.Int).Value = ItemsPerPage;
		    cmd.Parameters.Add("@TotalRows", SqlDbType.Int).Direction = ParameterDirection.Output;
		    cmd.Connection = con;
		    try
		    {
		        con.Open();
		        sda.SelectCommand = cmd;
		        sda.Fill(dt);
		        DataList1.DataSource = dt;
		        DataList1.DataBind();
		        TotalRows = Convert.ToInt32(cmd.Parameters["@TotalRows"].Value);
		    }
		    catch (Exception ex)
		    {
		        Response.Write(ex.Message);
		    }
		    finally
		    {
		        con.Close();
		        sda.Dispose();
		        con.Dispose();
		    }
		    return TotalRows;
		}
*/


	}
}