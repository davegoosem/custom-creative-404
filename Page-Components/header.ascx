﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="creative_404.Page_Components.header" %>

<div class="headerMain">
    <div class="headerWidth">
      	<div class="logo">
	        <div class="navMenu">
	          	<nav class="library">
		            <div class="column size1of1">
		            	<h3 class="styleGuideTitle">Creative 404.com (Under Construction)</h3> <!-- Could be replaced with logo -->
		                <ul class="nav">
		                  <li><a href="/home.aspx">Home</a></li>

		                  <li><a href="/gallery.aspx">Gallery</a></li>

		                  <li><a href="/about.aspx">About</a></li>

		                  <li><a href="/contact.aspx">Contact</a></li>

		                  <li><a href="/Account/Register.aspx">Register</a></li>

		                  <li><a href="/donate.aspx">Donate</a></li>
		                </ul>
		            </div>
	            	<div class ="welcomeText">
	            		<span runat="server" id="welcomeText">Welcome back!</span>
	            	</div>
	            	<div class="login-links">
		            	<asp:linkButton runat="server" Id="lnkLogin" text="Login" onclick="lnkLogin_Click"/>
		            </div>
	          	</nav>
	        </div>
      	</div>
    </div>
</div>