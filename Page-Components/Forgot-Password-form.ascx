<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Forgot-Password-Form.ascx.cs" Inherits="creative_404.Page_Components.Forgot_Password_Form" %>

<div class="login-form">
    <div class="form-title">
        <h2>Enter your registered email address below.</h2>
        <p class="form-text">Your password will be sent to your registered email address. Please ensure your spam filters are not blocking the message.</p>
    </div>

    <div class="form-wrapper">
        <div class="control-group">
            <span class="form-label">Email:</span>
                <asp:TextBox ID="TxtUserName" TabIndex="2" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RegularExpressionValidator ID="EmailRegExValidator" runat="server" ControlToValidate="TxtUserName" ErrorMessage="Please enter a valid email address." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" ValidationGroup="vgForgotPassword"> 
                    </asp:RegularExpressionValidator>
                </span>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="EmailNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtUserName" runat="server" ErrorMessage="Your email address is required." ValidationGroup="vgForgotPassword">
                    Your email address is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
        
        <asp:Button ID="ForgotPasswordButton" runat="server" CssClass="form-button" Text="Submit" onclick="ForgotPasswordButton_Click" ValidationGroup="vgForgotPassword"/>

        <asp:label class="validation-text" id="lblForgotPasswordMessage" runat="server"></asp:label>

    </div>
</div>