﻿<div class="footer-wrapper">
	<div class="footer-content">
		<div class="row-fluid">
			<div class="span6">	
				<div class="row-fluid">
					<div class="span5 footer-links"> 
						<h3>Account</h3>
						<ul>
							<li><a href="/Account/login.aspx">Login</a> </li>
							<li><a href="/Account/ChangePassword.aspx">Change Password</a> </li>
							<li><a href="/Account/ForgotPassword.aspx">Forgot Password</a> </li>
							<li><a href="/Account/Register.aspx">Register</a> </li>
						</ul>
					</div>
					<div class="span5 footer-links"> 
						<h3>Users</h3>
						<ul>
							<li><a href="/submit-your-page.aspx">Submit your page</a> </li>
							<li><a href="/gallery.aspx">View the gallery</a> </li>
							<li><a href="/contact.aspx">Contact</a> </li>
						</ul>
					</div>
				</div>

			</div>
			<div class="grid_6">  <h3>Search the site</h3>
				<p> Search Form will go here...</p>
			</div>
		</div>
		
		<div class ="footer-bottom-links">
			<div class="row-fluid">
				
				<div class="span4">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4eb57b3207ef4086"></script>
					<!-- AddThis Button END -->
				</div>

				<div class="span8">
					
					<div class="copyright">
						&copy; Copyright David Goosem 2012. All rights reserved.
					
					
					<!-- <div class="footer-bottom-links-right"> -->
						<span>
							<a href="/contact.aspx">Contact</a>
							 &nbsp;&nbsp; | &nbsp;&nbsp; <a href="/donate.aspx">Donate</a>
							 &nbsp;&nbsp; | &nbsp;&nbsp; <a href="/gallery.aspx">Gallery</a>
						</span>
					<!-- </div> -->
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

</form>

