using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;

namespace creative_404.Page_Components
{
	public partial class Forgot_Password_Form : System.Web.UI.UserControl
	{
		string newPassword = "";

		protected void ForgotPasswordButton_Click(object sender, EventArgs e)
		{
			try
			{
				SqlConnection conn = new SqlConnection(GetConnectionString());

				string checkExistsQuery = "SELECT UserName FROM Users WHERE Username = @username";

				SqlCommand checkExistsCmd = new SqlCommand(checkExistsQuery, conn);

				SqlParameter c1 = new SqlParameter("username", TxtUserName.Text);

				checkExistsCmd.Parameters.Add(c1);

				conn.Open();

				SqlDataReader rd = checkExistsCmd.ExecuteReader();
			    
				if (rd.HasRows)
				{
					string updatePasswordQuery = "UPDATE Users SET Password = @password";
					SendPassword();
					//need to update password in db here.
					SqlCommand updatePasswordCmd = new SqlCommand(updatePasswordQuery, conn);

					SqlParameter u1 = new SqlParameter("password", CalculateMD5Hash(newPassword));

					updatePasswordCmd.Parameters.Add(u1);

					rd.Close();
					updatePasswordCmd.ExecuteNonQuery();

					TxtUserName.Text = "";
					lblForgotPasswordMessage.Text="Your password has been sent to your email address.";					
					conn.Close();
				}
				else
				{
					lblForgotPasswordMessage.Text="Email address not found. Did you register another one?";
					rd.Close();
					conn.Close();
				}
			}
			catch (Exception) 
			{
				lblForgotPasswordMessage.Text="Unable to send your password at this time.";
			}
		}

		protected void SendPassword()
		{
			// Email Address from where you send the mail
			var fromAddress = "contact.creative404@gmail.com";
			// any address where the email will be sending
			var toAddress = TxtUserName.Text.ToString(); 
			//Password of your gmail address
			const string fromPassword = "cRe@t1ve!";
			// Passing the values and make a email formate to display
			string subject = "Forgot password request from Creative-404.com";
			string body = "You have received this message as a result of a password request being submitted with this email address on creative-404.com" + "\n";
			//run generate password and conver to string.
			string strNewPassword = GeneratePassword().ToString();
			newPassword = strNewPassword;

			body += "\n";
			body += "Username: " + TxtUserName.Text + "\n";
			body += "Password: " + strNewPassword + "\n";
			body += "\n";
			body += "You can log in with this new password and use the Change Password option to change it to whatever you'd like." + "\n";
			body += "\n";
			body += "Regards," + "\n";
			body += "\n";
			body += "creative-404.com";
			
			// smtp settings
			var smtp = new System.Net.Mail.SmtpClient();
			{
				smtp.Host = "smtp.gmail.com";
				smtp.Port = 587;
				smtp.EnableSsl = true;
				smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
				smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
				smtp.Timeout = 20000;
			}
			// Passing values to smtp object
			smtp.Send(fromAddress, toAddress, subject, body);
		}

		public string GetConnectionString()
		{
			return System.Configuration.ConfigurationManager.ConnectionStrings["Creative404ConnectionString"].ConnectionString;
		}

		public string GeneratePassword()
		{
		   //passwords will contain numbers, letters and special chars

		    string PasswordLength = "12";
		    string NewPassword = "";

		    //Define which characters are allowed in this new password
		    string allowedChars = "";			
		    allowedChars = "1,2,3,4,5,6,7,8,9,0";	
		    allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
		    allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
		    allowedChars += "~,!,@,#,$,%,^,&,*,+,?";

		    char[] sep = { ',' };
		    string[] arr = allowedChars.Split(sep);

		    string IDString = "";
		    string temp = "";

		    Random rand = new Random();	

		    for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)
		    {
		        temp = arr[rand.Next(0, arr.Length)];
		        IDString += temp;
		        NewPassword = IDString;

		        lblForgotPasswordMessage.Text = "new pass: " + NewPassword;
		    }

		    return NewPassword;
		}

		public string CalculateMD5Hash(string input)
		{
		    // step 1, calculate MD5 hash from input
		    MD5 md5 = System.Security.Cryptography.MD5.Create();
		    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
		    byte[] hash = md5.ComputeHash(inputBytes);
		 
		    // step 2, convert byte array to hex string
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < hash.Length; i++)
		    {
		        sb.Append(hash[i].ToString("X2"));
		    }
		    return sb.ToString();
		}

	}
}