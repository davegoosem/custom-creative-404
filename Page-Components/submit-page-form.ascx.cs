using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace creative_404.Page_Components
{
	public partial class submit_page_form : System.Web.UI.UserControl
	{

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			string imageFolder = "Gallery";
			string saveThumbPath;
			string saveFullPath;
			string saveThumbFile;
			string saveFullFile;
			string thumbFileName;
			string fullFileName;
			DateTime currentDateTime = DateTime.Now;
			string dateTimeNameValue = string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", currentDateTime);

			string insertImageQuery = "INSERT INTO ImageGallery (FullSizeFilePath, ThumbFilePath, FullSizeFileName, ThumbFileName, Website, Uploaded, UploaderID) VALUES "+ " (@saveFullPath, @saveThumbPath, @saveFullFile, @saveThumbFile, @website, @uploaded, @uploaderID)";

			if (Session["UserName"] != null)
			{
				SqlConnection conn = new SqlConnection(GetConnectionString());
				SqlCommand insertCmd = new SqlCommand(insertImageQuery, conn);
				SqlParameter[] param = new SqlParameter[7];

				conn.Open();

				if (inputThumbImage.HasFile && inputFullImage.HasFile)
				{
			        //set folder to unique value using date/time stamp.
					saveThumbPath = ("~/" + imageFolder + "/" + dateTimeNameValue + "/thumb");

					if (Directory.Exists(saveThumbPath))
					{	
						lblMessage.Text="This folder already exists.. Glitch in the Matrix?";
						return;
					}
					else
					{
						Directory.CreateDirectory(Server.MapPath(saveThumbPath));
						thumbFileName = inputThumbImage.FileName;
						saveThumbFile = Path.Combine(saveThumbPath, inputThumbImage.FileName);
						inputThumbImage.SaveAs(Server.MapPath(saveThumbFile));	

						saveFullPath = ("~/" + imageFolder + "/" + dateTimeNameValue + "/full");
						Directory.CreateDirectory(Server.MapPath(saveFullPath));						
						fullFileName = inputFullImage.FileName;
						saveFullFile = Path.Combine(saveFullPath, inputFullImage.FileName);
						inputFullImage.SaveAs(Server.MapPath(saveFullFile));
					}
					
					param[0] = new SqlParameter("@website", SqlDbType.VarChar, 50);
					param[1] = new SqlParameter("@saveFullPath", SqlDbType.VarChar, 200);
					param[2] = new SqlParameter("@saveThumbPath", SqlDbType.VarChar, 200);
					param[3] = new SqlParameter("@saveFullFile", SqlDbType.VarChar, 50);
					param[4] = new SqlParameter("@saveThumbFile", SqlDbType.VarChar, 50);
					param[5] = new SqlParameter("@uploaded", SqlDbType.DateTime);
					param[6] = new SqlParameter("@uploaderID", SqlDbType.VarChar, 50);


					param[0].Value = TxtWebsite.Text;
					param[1].Value = saveFullPath;
					param[2].Value = saveThumbPath;
					param[3].Value = fullFileName;
					param[4].Value = thumbFileName;
					param[5].Value = currentDateTime;
					param[6].Value = (Session["UserName"]);

			        for (int i = 0; i < param.Length; i++)
			        {
			            insertCmd.Parameters.Add(param[i]);
			        }

					insertCmd.CommandType = CommandType.Text;
					insertCmd.ExecuteNonQuery();

					lblMessage.Text="Files Successfully uploaded!";
					conn.Close();
					
				}
			}
			
			else
			{
				lblMessage.Text="You must be logged in to perform this action.";
			}
		}

		public string GetConnectionString()
		{
			return System.Configuration.ConfigurationManager.ConnectionStrings["Creative404ConnectionString"].ConnectionString;
		}

	}
}