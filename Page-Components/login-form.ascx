﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="login-form.ascx.cs" Inherits="creative_404.Page_Components.login_form" %>

<div class="login-form">
    <div class="form-title">
        <h2>Please login using your email and password.</h2>
    </div>
 
        <div class="control-group">
            <span class="form-label">Email:</span>
                <asp:TextBox ID="TxtUserName" TabIndex="2" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RegularExpressionValidator ID="EmailRegExValidator" runat="server" ControlToValidate="TxtUserName" ErrorMessage="Please enter a valid email address." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" ValidationGroup="vgLogin"> 
                    </asp:RegularExpressionValidator>
                </span>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="EmailNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtUserName" runat="server" ErrorMessage="Your email address is required." ValidationGroup="vgLogin">
                    Your email address is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

         <div class="control-group">
             <span class="form-label">Password:</span>
                <asp:TextBox ID="TxtPassword" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="200" TextMode="Password"></asp:TextBox>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="PasswordNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtPassword" runat="server"  ErrorMessage="A password is required." ValidationGroup="vgLogin">
                    A password is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
       
        <asp:Button ID="LoginButton" runat="server" CssClass="form-button" Text="Login" onclick="LoginButton_Click" ValidationGroup="vgLogin"></asp:Button>

        <asp:label class="validation-text" id="lblMessage" runat="server"></asp:label>

        <p><a class="forgot-password" href="/Account/ChangePassword.aspx">Change password</a></p>

    </div>
</div>
