using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace creative_404.Page_Components
{
	public partial class header : System.Web.UI.UserControl
	{

        protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["UserName"] == null)
            {
                lnkLogin.Text = "Login";
                welcomeText.Visible = false;
            }
            else
            {
                lnkLogin.Text = "Logoff";
                welcomeText.Visible = true;
            }
		}

	    protected void lnkLogin_Click(object sender, EventArgs e)
		{
			
            if (lnkLogin.Text == "Logoff")
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect(Request.RawUrl);
                lnkLogin.Visible = true;
            }
            else
            {
                Response.Redirect("/account/login.aspx");
            }
		}
	}
}