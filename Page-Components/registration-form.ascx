﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="registration-form.ascx.cs" Inherits="creative_404.Page_Components.registration_form" %>
<!-- header ASP.NET control -->

<div class="login-form">
    <div class="form-title">
        <h2>Please complete the form below to register.</h2>
        <p class="form-text">Registering will allow you to post comments and submit pages for consideration on the site.</p>
    </div>

    <div class="form-wrapper">
        <div class="control-group">
            <span class="form-label">Full Name:</span>
                <asp:TextBox ID="TxtName" TabIndex="1" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="NameNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtName" runat="server" ErrorMessage="Your name is required." ValidationGroup="vgRegistration">
                    Your name is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

        <div class="control-group">
            <span class="form-label">Email:</span>
                <asp:TextBox ID="TxtUserName" TabIndex="2" runat="server" CssClass="NormalTextBox" Width="200"></asp:TextBox>
                <span class="validation-text">
                    <asp:RegularExpressionValidator ID="EmailRegExValidator" runat="server" ControlToValidate="TxtUserName" ErrorMessage="Please enter a valid email address." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"> 
                    </asp:RegularExpressionValidator>
                </span>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="EmailNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtUserName" runat="server" ErrorMessage="Your email address is required." ValidationGroup="vgRegistration">
                    Your email address is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

         <div class="control-group">
             <span class="form-label">Password:</span>
                <asp:TextBox ID="TxtPassword" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="200" TextMode="Password"></asp:TextBox>
                <span class="validation-text">
                    <asp:RequiredFieldValidator ID="PasswordNotNullValidator" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtPassword" runat="server"  ErrorMessage="A password is required." ValidationGroup="vgRegistration">
                    A password is required.
                    </asp:RequiredFieldValidator>
                </span>
        </div>
            

       <div class="control-group">
             <span class="form-label">Retype Password:</span>
                <asp:TextBox ID="TxtRePassword" TabIndex="3" runat="server" CssClass="NormalTextBox" Width="200" TextMode="Password"></asp:TextBox>
                <span class="validation-text">
                    <asp:CompareValidator ID="RePasswordConfirmation" Display="Dynamic" SetFocusOnError="true" ControlToValidate="TxtRePassword" ControlToCompare="TxtPassword" runat="server" ErrorMessage="Passwords do not match." ValidationGroup="vgRegistration">
                    Password confirmation does not match password.
                    </asp:CompareValidator>
                </span>    
        </div>
        
        <asp:Button ID="Button1" runat="server" CssClass="form-button" Text="Submit" onclick="Button1_Click" ValidationGroup="vgRegistration"/>

        <asp:label class="validation-text" id="lblRegistrationMessage" runat="server"></asp:label>

    </div>
</div>
