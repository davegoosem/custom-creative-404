﻿<%@ Register TagPrefix="Footer" TagName="TagFooter" src="~/Page-Components/footer.ascx" %>
<%@ Register TagPrefix="Header" TagName="TagHeader" src="~/Page-Components/header.ascx" %>
<%@ Register TagPrefix="Head" TagName="TagHead" src="~/Page-Components/head.ascx" %>

<!DOCTYPE html>
<!--[if IE 7]>         <html class="ie7"> <![endif]-->
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if gt IE 8]>--> 
<html>

	<Head:TagHead id="Head" title="head" runat="server"/>
	<body>
		<form id="Form1" runat="server">
			<Header:TagHeader id="Header" title="header title" runat="server"/>
			
			<div class="content-title-strip">
				<div class="inner-wrapper">
					<div class="content-title">
						<div class="title-text">
							<h1>Donations welcome</h1>
							<p><h3>Everyone needs to believe in something. I believe I'll have another beer.</h3><p>
						</div>
					</div>
					<div class="content-title-image">
						<img src="/Styles/images/header-images/donate.png" alt="about-us icon"/>
					</div>
				</div>
			</div>


			<!-- Content section -->
			<div class="login-form">
				<div class="normal-heading">
			    	<h2>Development requires beer... &nbsp; &nbsp; &nbsp; Beer requires a beef fund... &nbsp; &nbsp; &nbsp;  Donate, go on...</h2>
			    </div>
			    
			    <div class="paypal-form">
			    	<p> It's thirsty work developing and maintaining a website. If you like the site and would like to see more work done on it, please throw a few bucks into the beer fund by clicking the delicious, foam-covered <strike>beer</strike> button below. It's quick and painless, using <a href="https://www.paypal.com" target="_blank">paypal</a>.</p>

			    	<p> If you're super keen, you can aslo let me know what you would like to see on the site by shooting me an email on the <a href="/contact.aspx">contact</a> page.</p>

					<span class="donate-button">
						<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=dgoosem%40gmail%2ecom&lc=AU&item_name=Creative%2d404%2ecom&currency_code=AUD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted" target="_blank" class="form-button">donate</a>
					</span>
			    </div>

			    <img src="/Styles/images/404-images/Beer-404.png" alt="Beer 404 page" class="donate-content-image"/>

			</div>
			<!-- end content section -->

			<Footer:TagFooter id="Footer" title="footer title" runat="server"/>
		</form>
	</body>
</html>